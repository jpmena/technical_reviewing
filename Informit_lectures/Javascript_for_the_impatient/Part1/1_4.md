* We can affect to a variable a value of a different type
  * There is no type inference
```javascript
> let d = 42
undefined
> typeof(d)
'number'
> d = {name:'MENA'}
{ name: 'MENA' }
> typeof(d)
'object'
> let e /** We don't affect a value, it canot infer the type */
undefined
> typeof(e)
'undefined'
```
