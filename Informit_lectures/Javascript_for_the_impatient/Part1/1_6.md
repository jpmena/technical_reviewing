```javascript
> const notQuitPI = parseFloat('3.14')
undefined
> const evenLessPI = parseInt('3.14')
> notQuitPI
3.14
> evenLessPI
3
> typeof(notQuitPI)
'number'
> typeof(evenLessPI)
'number'
 const evenLessPIStr = evenLessPI.toString()
undefined
> evenLessPIStr
'3'
> typeof(evenLessPIStr)
'string'
```
* Manipulating strings
```javascript
> 'Hello JPM'.substring(0, 2.9)
'He'
> 'Hello JPM'[2]
'l'
> 'Hello JPM'[4]
'o'
> 'Hello JPM'[2.9]
undefined
```
* playing with the 0 division
```javascript
> 3/0
Infinity
> typeof(3/0)
'number'
> -3/0
-Infinity
> typeof(-3/0)
'number'
> 0/0
NaN
> typeof(0/0)
'number'
```