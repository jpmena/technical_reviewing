# Template literals
* double template literal (a template literal inside another one)
```javascript
> let firstname = 'Jean-Pierre'
undefined
> let lastname = 'MENA'
undefined
> greeting = `Hello ${firstname.length >= 6? `${firstname[0]}+ '-' ${firstname[5]}`: ''}`
"Hello J+ '-' P" //It writes everything even the '
> greeting = `Hello ${firstname.length >= 6? `${firstname[0]}-${firstname[5]}`: ''}`
'Hello J-P'
```