# 50: do I have to install adb on Windows ?
* Go to *A propos de l'appareil* (About the device)
* Then click many times on *Version MUI* (May Build Number label)
  * The device helps you telling you have to still click x times before becoming a developper
  * It the announces you: *You are a developper
* I then have to restart Settings
* The Go to *Pametres supplémentaires* (Other parameters)
* And finally to *Options Developpeurs* (Developer Options)
  * I can swithch Off the Developper Options at any time 
  * I allow the USB Debugging (I can put it off at any time)
# TODO I don't see my Device in Android Studio
* see the [official documentation](https://developer.android.com/studio/run/device#setting-up)
  * part Windows
# That day (17/10/2024) Xiomi appears
* In the Device Manager Screen
  * After On my Adroid Pysical Xiaomi I have been asked to accept the Encryption key
* I select the physical Xiaomi Android Device and then
  * click the Run or the Debug icon see [Adding Icon Chapter](./4-AddingIcon.md)