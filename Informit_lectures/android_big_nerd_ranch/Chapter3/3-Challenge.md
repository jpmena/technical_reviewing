# 68 Before the Challenges (TODO)
* copy under Windows the main project to create a Challenge project
# 68 Challenge 1
* In the Question Object add a boolean read or use a decorator to add that boolean (already answered)
# 68 Challenge 2
* add a counter for the right answer before going back to 0, display that good answers counter (or a percentage)
# For the Solution
* see [MainActivity.kt](./AnswerToChallenges/MainActivity.kt) 
* and the Main ressource (notably for the previous Button): [activity_main](./AnswerToChallenges/activity_main.xml) 
