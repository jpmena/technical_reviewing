# Exploring ans testing the [Android Big Herd Ranch Guide 5th Edition](https://www.informit.com/store/android-programming-the-big-nerd-ranch-guide-9780137645602)
* This [5th Edition](https://www.informit.com/store/android-programming-the-big-nerd-ranch-guide-9780137645602) uses [Kotlin](https://kotlinlang.org/) as programming language since [JetPAck Compose](https://developer.android.com/compose) is written in Kotlin and can only be used in Kotlin.
# solutions to the challenges
* There are at least two GitHub Accounts which propose solutions to the challenges
  * [GitHub for an old edition](https://github.com/mtsahakis/NerdRanch)
    * the files date back 8 years ago (we are in September 2024)
  * [GitHub for the 3rd Edition](https://github.com/przemyslawmoskal/Big-Nerd-Ranch-Android)
    * the files date back 6 years ago (we are in September 2024)
  * [GitHub for the 4th Edition](https://github.com/AxiomeCG/BigNerdRanch-Android-4th-Edition/)
    * the files date back 2 years ago (we are in September 2024)
* ask questions to the [BigNerd Forum](https://forums.bignerdranch.com/)
  * the forum of this Book is [here](https://forums.bignerdranch.com/c/android-programming-5th-edition/782)
