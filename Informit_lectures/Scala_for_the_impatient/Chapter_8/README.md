# Exercices
* The exercises are at [Basile Du Plessis](https://github.com/BasileDuPlessis/scala-for-the-impatient)
  * Especially for [the Chapter 8](https://github.com/BasileDuPlessis/scala-for-the-impatient/tree/master/src/main/scala/com/basile/scala/ch08)