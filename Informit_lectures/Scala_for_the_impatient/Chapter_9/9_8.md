```scala
package com.absile.scala.chap09

object Test9_8 extends App{
  @SerialVersionUID(42L) class Person(val name:String, val age:Int) extends Serializable{
    override def toString: String = s"[Person] name ${name} with age $age%6d"
  }

  val fred = new Person("MENA", 56)
  import java.io._
  val out = new ObjectOutputStream(new FileOutputStream("/home/jmena01/ERICA/TOPAD/TOPAD-Cible/temp_person.obj"))
  out.writeObject(fred)
  out.close()
}
```
## 2 remarks
* the annotation is not sufficient to the calss Person
  * il must extends Serializable
  * otherwise an exeption __not serializable__ will be thrown 
* in /tmp the file does not appear, it must be put in a standard directory
```bash
jmena01@M077-1840900:~/ERICA/TOPAD/TOPAD-Cible$ cat temp_person.obj 
��sr&com.absile.scala.chap09.Test9_8$Person*IageLnametLjava/lang/String;xp8tMENAjmena01@M077-1840900:~/ERICA/TOPAD/TOPAD-Cible$
```
# To retore the Person class
* It must be the same package and className when saving as for restoring
## the Person class
* It is put apart
```scala
package com.absile.scala.chap09

@SerialVersionUID(42L) class Person(val name:String, val age:Int) extends Serializable{
  override def toString: String = s"[Person] name ${name} with age $age%6d"
}
```
## Saving
```scala
package com.absile.scala.chap09

object Test9_8 extends App{

  val fred = new Person("MENA", 56)
  import java.io._
  val out = new ObjectOutputStream(new FileOutputStream("/home/jmena01/ERICA/TOPAD/TOPAD-Cible/temp_person.obj"))
  out.writeObject(fred)
  out.close()
}
```
* in the fileSystem
```bash
jmena01@M077-1840900:~/ERICA/TOPAD/TOPAD-Cible$ cat temp_person.obj 
��srcom.absile.scala.chap09.Person*IageLnametLjava/lang/String;xp8tMENA
```
## Restoring
```scala
package com.absile.scala.chap09

import java.io.{FileInputStream, ObjectInputStream}

object Test9_8_1 extends App{
  val in = new ObjectInputStream(new FileInputStream("/home/jmena01/ERICA/TOPAD/TOPAD-Cible/temp_person.obj"))
  val savedFred = in.readObject().asInstanceOf[Person]
  println(s"on a retrouvé: |$savedFred|")
}
```
# When a person can have friends
## Person class
```scala
package com.absile.scala.chap09

import scala.collection.mutable

@SerialVersionUID(42L) class Person(val name:String, val age:Int) extends Serializable{
  private val friends = new mutable.ArrayBuffer[Person]
  def addFriend(p:Person):Unit = {
    friends += p
  }
  override def toString: String = {
    var res:String = s"[Person] name ${name} with age $age with ${friends.size} friends"
    for(p<- friends){
      res += s"\n [friend]$p"
    }
    res
  }
}
```
## Serializing program
```scala
package com.absile.scala.chap09

object Test9_8 extends App{

  val fred = new Person("MENA", 56)
  fred.addFriend(new Person("ANNE", 53))
  fred.addFriend(new Person("FABIENNE", 63))
  import java.io._ //import FileOutputStreal and ObjctOutputStrea
  val out = new ObjectOutputStream(new FileOutputStream("/home/jmena01/ERICA/TOPAD/TOPAD-Cible/temp_person.obj"))
  out.writeObject(fred)
  out.close()
}
```
## Deserializing program
```scala
package com.absile.scala.chap09

import java.io.{FileInputStream, ObjectInputStream} //Another way of importing

object Test9_8_1 extends App{
  val in = new ObjectInputStream(new FileInputStream("/home/jmena01/ERICA/TOPAD/TOPAD-Cible/temp_person.obj"))
  val savedFred = in.readObject().asInstanceOf[Person]
  println(s"on a retrouvé: |$savedFred|")
}
```