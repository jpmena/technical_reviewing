* does not fully work !!!
```scala
package com.absile.scala.chap09

object Test9_11 extends App{
  println(s"[main] with indices to begin")
  val numItemPattern = "([0-9]+) ([a-z]+)".r
  for (m <-numItemPattern.findAllMatchIn("99 bottles, 98 glasses")){
    println(s"[main][loop][0] |${m.group(0)}| starts at ${m.start(0)} and ends at ${m.end(0)}") //Works
    println(s"[main][loop][1] |${m.group(1)}| starts at ${m.start(1)} and ends at ${m.end(1)}") //Works
    println(s"[main][loop][2] |${m.group(2)}| starts at ${m.start(2)} and ends at ${m.end(2)}") //Works
  }
  println(s"[main] with names that time")
  val namedItemPattern = """([0-9]+) ([a-z]+)""".r("num", "item")
  for (m <-numItemPattern.findAllMatchIn("99 bottles, 98 glasses")){
    println(s"[main][loop][0] |${m.group(0)}| starts at ${m.start(0)} and ends at ${m.end(0)}") //Works
    println(s"[main][loop][1] |${m.group("num")}| starts at ${m.start(1)} and ends at ${m.end(1)}") //Exception No group with name <num>
    println(s"[main][loop][2] |${m.group("item")}| starts at ${m.start(2)} and ends at ${m.end(2)}") //Exception No group with name <item>
  }
}
```
# The variable extraction work
```scala
package com.absile.scala.chap09

object Test9_11_1 extends App{
  val numItemPattern = "([0-9]+) ([a-z]+)".r
  val numItemPattern(num, item) = "99 bottles" // We declare the num and item String variable not the regexp variable
  println(s"[main] the number $num for item $item")
  for (numItemPattern(num, item) <- numItemPattern.findAllIn("99 bottles, 88 glasses")){
    println(s"[main][loop] number $num for item $item")
  }
}
```
* Other test (just before the exercises of Chapter 9)
```scala
package com.absile.scala.chap09

object Test9_11_1 extends App{
  val numItemPatternLong = "([0-9]+) ([a-z]+), ([0-9]+) ([a-z]+)".r
  val numItemPatternLong(num, item, num2, item2) = "99 bottles, 88 glasses" // We declare the num and item String variable not the regexp variable
  println(s"[main] the number $num for item $item autre num ${num2} pour ${item2}")
  val numItemPattern = "([0-9]+) ([a-z]+)".r
  for (numItemPattern(num, item) <- numItemPattern.findAllIn("99 bottles, 88 glasses")){
    println(s"[main][loop] number $num for item $item")
  }
}
```