# Ressource
## The Book I am Reading
* it is [React Quickly 2nd Edition from Manning](https://www.manning.com/books/react-quickly-second-edition)
  * The book has been published in __July 2023__ and is no longer a MEAP
## The code of the Book
* It is on [Github rq2e project](https://github.com/rq2e/rq2e/tree/main)
* The same examples can be directly been seen on a [browse website](https://reactquickly.dev/browse)