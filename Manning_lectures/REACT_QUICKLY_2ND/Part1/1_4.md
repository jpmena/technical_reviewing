# p 44/458
## Listing 1.2
```js
const domNode = document.getElementById("root");
const root = ReactDOM.createRoot(domNode);
```
* I need to create __root__ as a React's root holder for the application (_ReactDOM.createRoot_)
  * the React's root holder points to the DOM Node with a _root_ id (it could have been anything else)

# p 46/458
* any Local web Server on port 3000 will work
* node, python2, python3, php
  * because it is only client side code (HTML, CSS, Javascript)
```bash
#node
$ npx serve
## or to be more precise
$ npx http-server -p 3000
#python
## python 2
$ python -m SimpleHTTPServer 3000
## python 3
$ python -m http.server 3000
#Php
$ php -S localhost:3000
```