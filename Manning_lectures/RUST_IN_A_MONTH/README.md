## From the Manninn ebook _Learn Rust in a Month of Lunches_

* The book [Learn Rust in a Month of Lunches](https://www.manning.com/books/learn-rust-in-a-month-of-lunches)
* at the date of th _05/10/2023_ the book is in version 10
  * it is in fact a MEAP book of actually 546 pages
  * __MEAP__ means _Manning Early Access Programm_ which means you receive a new version of the book as soon as the writer has made significant approved changes and until it is ready for the print edition
  * each new version must be controlled and approved by [Manning](https://www.manning.com/).

## RUST playground

* I test all my examples on [the RUST Playground website](https://play.rust-lang.org/?version=stable&mode=debug&edition=2021)
  * This is what the book recommends