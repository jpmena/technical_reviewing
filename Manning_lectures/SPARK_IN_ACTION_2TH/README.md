# Organization of the code of this Book
## on project per chapter
* every chapter has its own GitHub reporitory
* For Chapter 1 is is [GitHub projet for Chapter 1](https://github.com/jgperrin/net.jgp.books.spark.ch01)
* for Chapter 15 it is [GitHub projet for Chapter 15](https://github.com/jgperrin/net.jgp.books.spark.ch15)
## Each project is now on Scala, Python and Java
* and no more only in Java !!!
* There is a [pom.xml (example of project 1)](https://github.com/jgperrin/net.jgp.books.spark.ch01/blob/master/pom.xml)
* But also a [build.sbt (example of projec 15)](https://github.com/jgperrin/net.jgp.books.spark.ch15/blob/master/build.sbt)
# Some technical remarks
* [The answer 39 of this StackOverflow Post](https://stackoverflow.com/questions/6051302/what-does-colon-underscore-star-do-in-scala) explains well what **:_*** means.
  * versy practical