# Resources
* Those are my notes fromthe [Manning Book Bayesian Optimisation in Action](https://www.manning.com/books/bayesian-optimization-in-action)
  * it comes as an extension of [my notes of the Bishop Bible of Gaussian processes](https://github.com/javaskater/BischopPRML) 
* The Examples are in Jupyter format on the [GitHub Project of the Author](https://github.com/KrisNguyen135/bayesian-optimization-in-action)