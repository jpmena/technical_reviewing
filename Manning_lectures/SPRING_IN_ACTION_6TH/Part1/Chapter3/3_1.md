# 90/520
## listing 3.2
* How we can retrieve eum object from their String representation
```java
jshell> public enum Type{
   ...>     WRAP,PROETIN,VEGGIS, CHESE, SAUCE
   ...> }
|  created enum Type
jshell> Type t = Type.valueOf("WRAP")
t ==> WRAP
```
# 91/520
* for the [class level Serialization ID](https://www.baeldung.com/java-serial-version-uid)
# 92/520
* to add the dependency
* right click on the project
   * Spring / Add Starters
   * open _SQL_ and choose _JDBC API_
   * push the difference to the Right
   * click finish
* same ting with _SQL_ and _H2 Database_
* It has added
```xml
<dependency>
   <groupId>org.springframework.boot</groupId>
   <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
   <groupId>com.h2database</groupId>
   <artifactId>h2</artifactId>
   <scope>runtime</scope>
</dependency>
```
* pto connect to the  H2 console the URL is [http://localhost:8080/h2-console/](http://localhost:8080/h2-console/)
## STS is very useful to edit a yaml file
* _taco-cloud/src/main/resources/application.properties_
```bash
spring.thymeleaf.cache=false
spring.datasource.generate-unique-name=false
spring.datasource.name=tacocloud
```
* becomes _taco-cloud/src/main/resources/application.yml_
```yml
spring:
  thymeleaf:
    cache: false
  datasource:
    generate-unique-name: false
    name: tacocloud
```
* STS helps a lot along with the yaml syntax (underlining in red the error and correctly shifting)
# 93/520
* the access UTL to the H2 tacocloud database is __jdbc:h2:mem:tacocloud__

# 94/520
* my STS dislike the _@Override_ annotation although they are automatically generated with the code ...
* the double colon __::__ is new to __Java 8__ and is mimicking a lambda expression
  * except that in this case the function to be called has already been defined
> as well as an implementation of Spring’s RowMapper for the purpose of mapping each row in the result set to an object
## What are Optionals
* optional has been introduced in __Java 8__ and is either present or absent
* if it is present the value it contains is obtained by the get Method
* see [developer.com](https://www.developer.com/java/java-optional-object/) for more informations

# 95/520
* __Java 8 method reference__ is the same as a lambda!!!
> RowMapper parameter for both findAll() and findById() is given as a method reference to the mapRowToIngredient() method. Java’s method references and lambdas are convenient when working with JdbcTemplate as an alternative to an explicit RowMapper implementation
* insead of a method reference I can use an anonymous inner class (see Android development) which gives in our case:
```java
@Override
public Ingredient findById(String id) {
   return jdbcTemplate.queryForObject(
      "select id, name, type from Ingredient where id=?",
      new RowMapper<Ingredient>() {
         public Ingredient mapRow(ResultSet rs, int rowNum)
         throws SQLException {
         return new Ingredient(
         rs.getString("id"),
         rs.getString("name"),
         Ingredient.Type.valueOf(rs.getString("type")));
      };
   }, id);
}
```
* it then replace  the private method:
```java
private Ingredient mapRowToIngredient(ResultSet row, int rowNum) throws SQLException{
	return new Ingredient(row.getString("id"), row.getString("name"), Ingredient.Type.valueOf(row.getString("type")));
}
```
# 96/520
* we insert the repository directly into the controller...