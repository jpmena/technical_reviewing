# p 128/167
* When we speak of space we indicate the entre length and wih of a row or column (track)
  * The Figure 5.3 explains it better than the textual description
# p 133/167
* figure 5.7 expalins well how
```css
grid-column: 1/3;
```
* is numbered
* They use the grid-line
  * a grid-line can be horizontally (_grid-row_ property)
  * a grid line can be vertically (_grid-column_ property)
# p 135/167
* fir the span property we use the notoin of track (see figure 5.3)
* _This tells the browser that the item will span one grid track_
# p 136/167
* __Figure 5.8__ explains better than text where flex and grid differ

# p 138/167
* Listing 5.5 ia a good revision of Chapter 4
  * _display: block;_ in the ul > li > a may be possible because we are i a flex layout (keeps things aligned horizontally)
  * if I comment out _display:flex_ each a will be on its own line
* Question _grow to fill the space,_ if you secify a flex property on the child element.
 * by default [MDN doc Initial value](https://developer.mozilla.org/en-US/docs/Web/CSS/flex) is grow 0, shrnk 1, basis auto
* nav right: lets th margin on the left fille the container taking into account the padding of the container
## case of the cost container
* [justifiy-content](https://developer.mozilla.org/en-US/docs/Web/CSS/justify-content) justifies along the main axis
  * _defines how the browser distributes space between and around content items along the main-axis of a flex container,_
* [align-items](https://developer.mozilla.org/en-US/docs/Web/CSS/align-items)
  * is best explained by the try-out on the previous link
  * the corresponding property applied to a single element is _align-self_
* Note that text-align does not apply to the cost container it uses fex's justifiy-content instead
  * but it applies to the _small_ and _a_ inline elements
* with the flexstart the upper bound of lineheight zont is too high
  * so you have to adjust the line-height
## case of the cta-button
* making a displayblock will make the .center property of text-align works inside the block which takes the full content's width
## Conclusion
* _When your design calls for an alignment of items in two dimensions, use a grid. When you’re only concerned with a one-directional flow, use flexbox._
  * grid high level layout
  * flexbox: certains elements within each grid area
