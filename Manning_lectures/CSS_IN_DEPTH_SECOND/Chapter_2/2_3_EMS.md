# Responsive base font size:
* p 52: _By applying these font sizes at the root on your page, you’ve responsively redefined the meaning of em and rem throughout the entire page._
  * all the root sizes are in term relative (id _em_) to the default navigator size (of usuallly 16px)
## In case of media Quesries:
* p 54/167: _if you don’t want your border radius to scale, define it using rems, and use ems only for the properties you want to make scalable_

