# Source
* This is my lecture of the Pack Book [Building Real-World Web Applications with Vue.js 3](https://www.packtpub.com/en-us/product/building-real-world-web-applications-with-vuejs-3-9781837630394)
* The code accompanying the book has its own [Github repository](https://github.com/PacktPublishing/Building-Real-world-Web-Applications-with-Vue.js-3)
* it is reommended to access the [Vue documentation](https://vuejs.org/guide/introduction.html) for a more in depth unnderstanding of all he conepts