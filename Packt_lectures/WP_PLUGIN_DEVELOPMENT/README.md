# work on the Book [Packt Publishing Wordpress Plugin Development 3rd Edition](https://www.packtpub.com/en-us/product/wordpress-plugin-development-cookbook-9781801810777)
* the [Packt Publishing Wordpress Plugin Development 3rd Edition book](https://www.packtpub.com/en-us/product/wordpress-plugin-development-cookbook-9781801810777)
  * that book dates back to 2022 
* the [corresponding Source Code hosted on gitHub](https://github.com/PacktPublishing/WordPress-Plugin-Development-Cookbook-Third-Edition)
# Using docker instead of [Local application from flywheel](https://www.localwp.com/)
* see [My documentation on docker](./Chap1/Docker/Docker.md)
# Official Docker documentation
* see page 27 of the book
* [Wordpress Codex](https://codex.wordpress.org/)
* [Wordpress Developper Reference](https://developer.wordpress.org/reference/)