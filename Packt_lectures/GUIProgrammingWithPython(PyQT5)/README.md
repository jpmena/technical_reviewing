# The Book I am reading
* Those are my notes from the Packt Book [Mastering GUI Programming with Python](https://www.packtpub.com/product/mastering-gui-programming-with-python/9781789612905)
* The source Code of that book is on [GitHub PackPub Site](https://github.com/PacktPublishing/Mastering-GUI-Programming-with-Python)