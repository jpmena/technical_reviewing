# Te Book and the Solutions
* This is the [Python Programming with Design Patterns](https://www.informit.com/store/python-programming-with-design-patterns-9780137579938) Informit book.
* The Solution i on the [Github page of jameswcooper](https://github.com/jwcnmr/jameswcooper/tree/main/Pythonpatterns)
  * Especially [chapter 2](https://github.com/jwcnmr/jameswcooper/tree/main/Pythonpatterns/2.%20Visual%20programming)
## Why Tkinter
* because it is in the code of the python 3 distribution (no need of an extra package like PyQT5)
* It is used in the following chapters to visually demonstrate some Design Patterns 