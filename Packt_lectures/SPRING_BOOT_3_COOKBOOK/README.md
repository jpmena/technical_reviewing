# My notes on the [Spring-Boot-3.0-Cookbook Packt Book](https://github.com/PacktPublishing/Spring-Boot-3.0-Cookbook)
## p xvii
* the books examples are made on *windows / WSL / Docker on windows*
* the jdk are 11, 17 and 21
* the author uses [Visual Studio Code](https://code.visualstudio.com/)
## The [source code for the book](https://github.com/PacktPublishing/Spring-Boot-3.0-Cookbook)
* The source code used in the book is hosted [on GitHub Packt](https://github.com/PacktPublishing/Spring-Boot-3.0-Cookbook)