# Resources
* **ERRATUM** [start/end of the recipe not found on GitHub](https://github.com/PacktPublishing/Spring-Boot-3.0-Cookbook/tree/main/chapter1)
## See [RECIPE 7 test part](./Recipe7.md)
* Already done in the Test part of [Recipe 7](./Recipe7.md)
* The code is on [ test Class GitHub for RECIPE 7](https://github.com/PacktPublishing/Spring-Boot-3.0-Cookbook/blob/main/chapter1/recipe1-7/end/albums/src/test/java/com/packt/albums/FootballClientServiceTests.java)
# 33
## Author's note
> There is a known incompatibility with Spring Boot version 3.2.x. Spring Boot uses
> Jetty 12, while Wiremock depends on Jetty 11. To avoid that incompatibility we used the wiremock-standalone instead of the wiremock artifact,
> as it includes all required dependencies
