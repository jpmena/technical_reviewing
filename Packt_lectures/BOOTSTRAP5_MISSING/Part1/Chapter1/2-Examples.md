# 9 [Examples of user interfaces](https://github.com/PacktPublishing/The-Missing-Bootstrap-5-Guide/tree/main/part-1/chapter-1/example-user-interfaces
## The Card
* the .nav CSS class defines a lot of variables for example
* card-text class has no effect on the paragraph and is not detected by the Firefox inspector
* card-link has an effect on the links paddings/margins but is not detected by the Firefox inspector neither
  * not even in smartphone modus
# 14
## The Forum
* I do'nt copy the code too long
* I checked index, forum-category, forum-name
* **TODO** forum-topic.html (the adress in the Firefox address bar: *file:///home/jmena01/CONSULTANT/The-Missing-Bootstrap-5-Guide/part-1/chapter-1/example-user-interfaces/forum/forum-topic.html*)
# 17
* The portfolio example is very important for creating my online resumee