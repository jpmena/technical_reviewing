# The Missing Bootstrap 5 Guide
* This is my notes of the reading of the [Bootstrap 5 Missing manual from Packt](https://www.packtpub.com/en-us/product/the-missing-bootstrap-5-guide-9781801076432)
<a href="https://www.packtpub.com/product/the-missing-bootstrap-5-guide/9781801076432"><img src="https://static.packt-cdn.com/products/9781801076432/cover/smaller" alt="The Missing Bootstrap 5 Guide" height="256px" align="right"></a>

## The code for this Book 
* It is on Github at [Bootstrap 5 Missing Manual Code GitHub page](https://github.com/PacktPublishing/The-Missing-Bootstrap-5-Guide)