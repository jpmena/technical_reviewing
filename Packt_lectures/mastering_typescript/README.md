# Book and Code
* I am reading the  [Packt Book Mastering TypeScript - Fourth Edition](https://www.packtpub.com/product/mastering-typescript-fourth-edition/9781800564732)
* There is a [GitHub Code Repository](https://github.com/PacktPublishing/Mastering-TypeScript-Fourth-Edition) To Bee Used with the Book
* [Chapter 1](./Chapter1.md) is about installing typescrit