# The [Book](https://www.packtpub.com/en-us/product/vuejs-3-for-beginners-9781805126775)
* It is a Pckt Book [ Vue.js 3 for Beginners: Learn the essentials of Vue.js 3 and its ecosystem to build modern web applications](https://www.packtpub.com/en-us/product/vuejs-3-for-beginners-9781805126775)

# [source Code][https://github.com/PacktPublishing/Vue.js-3-for-Beginners]
* The example code is updated at the [Packt Github space](https://github.com/PacktPublishing/Vue.js-3-for-Beginners)
* I only see one project. It is the unique companion project of this book.
* Each chapter/part of the book correspond to a branch
  * by default  [the project](https://github.com/PacktPublishing/Vue.js-3-for-Beginners) opens to the last branch (CH11) 