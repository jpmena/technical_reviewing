# Origin
* this project is the result of My lectures
* [Manning](https://www.manning.com)
* [Packt Publishing](https://www.packtpub.com/)
* [Informit (Pearson , Wiley)](https://www.informit.com/)
# I share my experience with the different lectures/trainings
* Is includes 
  * remarks with links to the online documentation available in Markdown files
  * code samples in those same Markdown files or separately
# Problem when starting WSL/Visual Studio Code
* the [WSL Integration asks for update](https://github.com/microsoft/vscode-remote-release/issues/3839) (Downloading server)
* and you cannot do anything if you don't accept the Update
# To access th WSL/Ubuntu home directory
* from  Windows it is *\\wsl.localhost\Ubuntu\home\jpmena* to be passed in the explorer Adress Bar
* or when in WSL you can always call 
```bash
jpmena@LAPTOP-E2MJK1UO:~$ explorer.exe .
```
* which opens on Windows the Windows Explorer at the *\\wsl.localhost\Ubuntu\home\jpmena* address